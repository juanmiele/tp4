﻿using Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Concecionario
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            Enlazar();
        }
        private void limpiar()
        {

            textBox1.Text = string.Empty;
            textBox2.Text = string.Empty;
            textBox3.Text = string.Empty;
            textBox4.Text = string.Empty;
            textBox5.Text = string.Empty;
            textBox6.Text = string.Empty;
            textBox7.Text = string.Empty;

        }
        private void Enlazar()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = Cliente.listar();
            //comboBox1.DataSource = null;
            //comboBox1.DataSource = Cliente.listar();
            //comboBox2.DataSource = null;
            //comboBox2.DataSource = Auto.ListarTodos();
            dataGridView2.DataSource = null;
            dataGridView2.DataSource = Auto.ListarTodos();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Cliente cliente = new Cliente();
            try
            {
                ValidarCliente();
                Cliente.Alta(textBox1.Text, textBox2.Text, float.Parse(textBox3.Text));
                limpiar();
                Enlazar();


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Auto auto = new Auto();
            try
            {
                ValidarAuto();
                Auto.Alta(textBox4.Text, int.Parse(textBox5.Text), textBox6.Text,int.Parse(textBox7.Text));
                limpiar();
                Enlazar();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                ValidarVenta();
                Cliente cliente = (Cliente)dataGridView1.CurrentRow.DataBoundItem;
                Auto auto = (Auto)dataGridView2.CurrentRow.DataBoundItem;
                Venta.Alta(cliente.Id, auto.Id);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
     
        }
        private void ValidarVenta()
        {


            Auto auto = (Auto)dataGridView2.CurrentRow.DataBoundItem;

            if (auto == null)
            {
                throw new Exception("Seleccione un Auto para poder confirmar la venta");
            }

            Cliente cliente = (Cliente)dataGridView1.CurrentRow.DataBoundItem;

            if (cliente == null)
            {
                throw new Exception("Seleccione un Cliente para poder confirmar la venta");
            }
        }
        private void ValidarCliente()
        {
            float dni;

            if (!float.TryParse(textBox3.Text, out dni))
            {
                throw new Exception("El valor ingresado para DNI no es correcto");
            }

            if (string.IsNullOrEmpty(textBox1.Text))
            {
                throw new Exception("El campo Apellido esta incompleto");
            }

            if (string.IsNullOrEmpty(textBox2.Text))
            {
                throw new Exception("El campo Nombre esta incompleto");
            }
        }

        private void ValidarAuto()
        {
            float año;
            float precio;

            if (!float.TryParse(textBox5.Text, out año))
            {
                throw new Exception("El valor ingresado para año no es correcto");
            }

            if (string.IsNullOrEmpty(textBox4.Text))
            {
                throw new Exception("El campo Patente esta incompleto");
            }

            if (string.IsNullOrEmpty(textBox6.Text))
            {
                throw new Exception("El campo patente esta incompleto");
            }
            if (!float.TryParse(textBox7.Text, out precio))
            {
                throw new Exception("El valor ingresado para Precio no es correcto");
            }
        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void Estado_Click(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
