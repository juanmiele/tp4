﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace Negocio
{
    public class Auto
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        private string patente;

        public string Patente
        {
            get { return patente; }
            set { patente = value; }
        }

        private int año;

        public int Año
        {
            get { return año; }
            set { año = value; }
        }

        private string marca;

        public string Marca
        {
            get { return marca; }
            set { marca = value; }
        }


        private int precio;

        public int Precio
        {
            get { return precio; }
            set { precio = value; }
        }




        public override string ToString()
        {
            return patente.ToUpper();
        }


        public static List<Auto> ListarTodos()
        {

            List<Auto> lista = new List<Auto>();
            Acceso acceso = new Acceso();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("Auto_Listar");
            acceso.Cerrar();
            



            foreach (DataRow registro in tabla.Rows)
            {
                Auto auto = new Auto();
                auto.id = int.Parse(registro["ID_Auto"].ToString());
                auto.patente = registro["Patente"].ToString();
                auto.año = int.Parse(registro["año"].ToString());
                auto.marca = registro["marca"].ToString();
                auto.precio = int.Parse(registro["precio"].ToString());
                lista.Add(auto);
            }
            return lista;
        }
        public static int Alta(string patente, int año, string marca, float precio)
        {
            Acceso acceso = new Acceso();
            List<SqlParameter> parametros = new List<SqlParameter>();

            SqlParameter paramPatente = new SqlParameter();
            paramPatente.ParameterName = "@patente";
            paramPatente.Value = patente;
            paramPatente.SqlDbType = SqlDbType.Text;
            parametros.Add(paramPatente);

            SqlParameter paramAño = new SqlParameter();
            paramAño.ParameterName = "@año";
            paramAño.Value = año;
            paramAño.SqlDbType = SqlDbType.Int;
            parametros.Add(paramAño);

            SqlParameter paramMarca = new SqlParameter();
            paramMarca.ParameterName = "@marca";
            paramMarca.Value = marca;
            paramMarca.SqlDbType = SqlDbType.Text;
            parametros.Add(paramMarca);

            SqlParameter paramPrecio = new SqlParameter();
            paramPrecio.ParameterName = "@precio";
            paramPrecio.Value = precio;
            paramPrecio.SqlDbType = SqlDbType.Int;
            parametros.Add(paramPrecio);

            acceso.Abrir();
            int exito = acceso.Escribir("auto_insertar", parametros);
            acceso.Cerrar();

            return exito;
        }
    }
}
