﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace Negocio
{
    public class Cliente
    {
        private int id;
        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }
        private string apellido;

        public string Apellido
        {
            get { return apellido; }
            set { apellido = value; }
        }
        private int dni;

        public int Dni
        {
            get { return dni; }
            set { dni = value; }
        }
        private Auto auto;

        public Auto Auto
        {
            get { return auto; }
            set { auto = value; }
        }
        public override string ToString()
        {
            return apellido.ToUpper();
        }

        public static List<Cliente> listar()
        {
            List<Cliente> lista = new List<Cliente>();


            Acceso acceso = new Acceso();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("CLIENTE_LISTAR");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                Cliente cliente= new Cliente();
                cliente.id = int.Parse(registro["ID_CLIENTE"].ToString());
                cliente.nombre = registro["NOMBRE"].ToString();
                cliente.apellido = registro["APELLIDO"].ToString();
                cliente.dni = int.Parse(registro["DNI"].ToString());
                lista.Add(cliente);
            }


            return lista;
        }
        public static int Alta(string nombre, string apellido, float dni)
        {
            Acceso acceso = new Acceso();
            List<SqlParameter> parametros = new List<SqlParameter>();

            SqlParameter paramApellido = new SqlParameter();
            paramApellido.ParameterName = "@ape";
            paramApellido.Value = apellido;
            paramApellido.SqlDbType = SqlDbType.Text;
            parametros.Add(paramApellido);

            SqlParameter paramNombre = new SqlParameter();
            paramNombre.ParameterName = "@nom";
            paramNombre.Value = nombre;
            paramNombre.SqlDbType = SqlDbType.Text;
            parametros.Add(paramNombre);

            SqlParameter pdni = new SqlParameter();
            pdni.ParameterName = "@dni";
            pdni.Value = dni;
            pdni.SqlDbType = SqlDbType.Float;
            parametros.Add(pdni);

            acceso.Abrir();
            int exito = acceso.Escribir("Cliente_insertar", parametros);
            acceso.Cerrar();

            return exito;
        }
    }
}
