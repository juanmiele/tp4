﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace Negocio
{
    public class Venta
    {
        private int id;
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private int idAuto;
        public int IdAuto
        {
            get { return idAuto; }
            set { idAuto = value; }
        }

        private int idCliente;
        public int IdCliente
        {
            get { return idCliente; }
            set { idCliente = value; }
        }

        
        public static int Alta(int idCliente, int idAuto)
        {
            Acceso acceso = new Acceso();
            List<SqlParameter> parametros = new List<SqlParameter>();

            SqlParameter paramCliente = new SqlParameter();
            paramCliente.ParameterName = "@Id_Cliente";
            paramCliente.Value = idCliente;
            paramCliente.SqlDbType = SqlDbType.Int;
            parametros.Add(paramCliente);

            SqlParameter paramAuto = new SqlParameter();
            paramAuto.ParameterName = "@Id_Auto";
            paramAuto.Value = idAuto;
            paramAuto.SqlDbType = SqlDbType.Int;
            parametros.Add(paramAuto);

            acceso.Abrir();
            int exito = acceso.Escribir("Venta_insertar", parametros);
            acceso.Cerrar();

            return exito;
        }
    }
}
